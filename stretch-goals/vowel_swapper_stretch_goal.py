def vowel_swapper(string):
    # ==============
    # Your code here

     string = string.replace("a", "4").replace("A", "4")
     string = string.replace("e", "3").replace("E", "3")
     string = string.replace("i", "!").replace("I", "!")
     string = string.replace("o", "ooo").replace("O", "000")
     string = string.replace("u", "|_|").replace("U", "|_|")
     
     return string

    # ==============

print(vowel_swapper("aAa eEe iIi oOo uUu")) # Should print "a/\a e3e i!i o000o u\/u" to the console
print(vowel_swapper("Hello World")) # Should print "Hello Wooorld" to the console 
print(vowel_swapper("Everything's Available")) # Should print "Ev3rything's Av/\!lable" to the console
