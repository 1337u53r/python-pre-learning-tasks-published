def calculator(a, b, operator):
    # ==============
    # Your code here
    
    if operator == "+":
        result = a + b
        
    elif operator == "-":
        result = a - b
        
    elif operator == "*":
        result = a * b
        
    elif operator == "/":
        result = int(a / b)
        
    return result

    # ==============

print(format(calculator(2, 4, "+"), "b")) # Should print 110 to the console
print(format(calculator(10, 3, "-"), "b")) # Should print 111 to the console
print(format(calculator(4, 7, "*"), "b")) # Should output 11100 to the console
print(format(calculator(100, 2, "/"), "b")) # Should print 110010 to the console
